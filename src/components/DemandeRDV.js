import React, { Component } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import TitleParagraph from './TitleParagraph'
import { render } from '@testing-library/react';
import GrandTitre from './GrandTitre';
import {Link, BrowserRouter } from 'react-router-dom' 
import axios from 'axios';
import TextField from '@material-ui/core/TextField';
import InsertInvitationIcon from '@material-ui/icons/InsertInvitation';
import InputLabel from '@material-ui/core/InputLabel';

export default class DemandeRDV extends Component {
  

  constructor(props) {
    super(props)
    this.onSubmit=this.onSubmit.bind(this) 
   
    this.state={
        idPatient:window.localStorage.getItem("idPatient"),
        idDoctor:this.props.match.params.id,
        appointmentDate:'',
        lastNamePatient: '',
        firstNamePatient: '',
        gender:'',
        validation:'',
        idDoctor:'',
        nomDoc:'',
        prenomDoc:'',
        appointments:[],
        appointmentDateString:''
     
      };
    
    }
    onSubmit(e){
        e.preventDefault();
        const appointment={
            appointmentDate:this.state.appointmentDate,
            idDoctor:this.props.match.params.id,
            idPatient:window.sessionStorage.getItem("idPatient"),
            lastNamePatient:this.state.lastNamePatient,
            firstNamePatient:this.state.firstNamePatient,
            appointmentDateString : this.state.appointmentDate,
            validation:0,
            gender:this.state.gender
             
          }
          console.log(appointment)
          axios.post('http://localhost:8080/appointment/add',appointment)
          .then(res=>{
            console.log(res.data)
            if(window.confirm('Votre demande a été envoyée vers l\'équipe docFinder. Voulez vous voir votre fiche de demande de Rendez-Vous?')){
                window.location='/rendezVous';
            }
            })
            .catch(error=>{
                console.log(error)
            }) 

        }
        demandeRDV(id){
    
            if(window.sessionStorage.getItem("idPatient")=='undefined'){
            alert('Veuillez-vous connecter d\'abord avant de demander un Rendez-vous')
          }
          else if(window.sessionStorage.getItem("idPatient")==null){
            alert('Veuillez-vous connecter d\'abord avant de demander un Rendez-vous')
          }
          else{
            //window.location.href="demandeRDV/"+id;
           // <Redirect to={`/demandeRDV/${id}`} />
           this.props.history.push("demandeRDV/"+id);
        
          }
        }       
     componentDidMount(){
        axios.get('http://localhost:8080/patient/'+window.sessionStorage.getItem("idPatient"))
        .then(response=>{
          console.log('ezzzbgrfg',response)
          this.setState({
           lastNamePatient: response.data.lastname,
            firstNamePatient:response.data.firstname,
            gender: response.data.gender
          
          
          })
        
        })
        .catch(error=>{
          
          console.log('ezzzbgrfg',error)
        })
        axios.get('http://localhost:8080/doctor/'+this.props.match.params.id)
        .then(response=>{
          console.log('ezzzbgrfg',response)
          this.setState({
           nomDoc: response.data.firstname,
            prenomDoc:response.data.lastname,
           })
        
        })
        .catch(error=>{
          
          console.log('ezzzbgrfg',error)
        })
     }
  
render(){
  return (
    <div>
      <br/> 
      <GrandTitre name="Demande de rendez-vous"/>
      <br/>
      <Grid container spacing={2}>
        
        <Grid item xs={3}>
          
        </Grid>
        <Grid item xs={4}>
           <br/>  <br/>    
          <Paper >
          <React.Fragment >
          <br/> 

          <form onSubmit={this.onSubmit}>
        
          <Typography variant="h6"><InsertInvitationIcon/> Demande de rendez-vous avec le docteur {this.state.nomDoc}&nbsp;{this.state.prenomDoc}</Typography>
            <InputLabel htmlFor="select">Veuillez choisir la date et Heure</InputLabel>
              <TextField
                autoComplete="fname"
                name="firstname"
                variant="outlined"
                required
                fullWidth
                id="date"
                type="datetime-local"
                onChange={(event) => this.setState({appointmentDate: event.target.value})}
              />
            <br/>  <br/> 
          <Button
            type="submit"
            
            variant="contained"
            color="primary"
           
          >
            Effectuer la demande
          </Button>
         
        </form>     
        </React.Fragment>
        <br/> 
    
        </Paper >
  
        
        </Grid>
        <Grid item xs={3}>
         
        </Grid>
      </Grid>
      <br/> 
      <br/> 
      <br/> 
      <br/> 
      
    </div>
  );
}
}