import React, { Component,useState} from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Button from '@material-ui/core/Button';
import TitleParagraph from './TitleParagraph'
import { render } from '@testing-library/react';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import GrandTitre from './GrandTitre';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import NativeSelect from '@material-ui/core/NativeSelect';
import axios from 'axios';
import Slider from '@material-ui/core/Slider';
import Checkbox from '@material-ui/core/Checkbox';
import Autocomplete from '@material-ui/lab/Autocomplete';
import CheckBoxOutlineBlankIcon from '@material-ui/icons/CheckBoxOutlineBlank';
import CheckBoxIcon from '@material-ui/icons/CheckBox';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import Divider from '@material-ui/core/Divider';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import Avatar from '@material-ui/core/Avatar';
import { Alert, AlertTitle } from '@material-ui/lab';
import RefreshIcon from '@material-ui/icons/Refresh';
import {Link} from 'react-router-dom'
import jsPDF from 'jspdf';  
import html2canvas from 'html2canvas';  
import GetAppIcon from '@material-ui/icons/GetApp';
import Fab from '@material-ui/core/Fab';
import DeleteIcon from '@material-ui/icons/Delete';
import IconButton from '@material-ui/core/IconButton';
import Tooltip from '@material-ui/core/Tooltip';
import EventAvailableIcon from '@material-ui/icons/EventAvailable';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';

export default class Diagnostic2 extends Component {
  

  constructor(props) {
    super(props)
    this.demandeRDV=this.demandeRDV.bind(this)
    this.onChangeAge=this.onChangeAge.bind(this)
    this.onChangeAutrecas=this.onChangeAutrecas.bind(this)
    this.onChangeCheckbox=this.onChangeCheckbox.bind(this)
    this.onChangeSexe=this.onChangeSexe.bind(this)
    this.onChangePartieducorps=this.onChangePartieducorps.bind(this)
    this.onChangeSymptome=this.onChangeSymptome.bind(this)
    this.onChangeSymptome1=this.onChangeSymptome1.bind(this)
    this.addSelect=this.addSelect.bind(this)
    this.onChangeSliderventre=this.onChangeSliderventre.bind(this)
    
    this.onSubmit=this.onSubmit.bind(this)

		this.state={
        age:'',
        partieducorps:'',
        autrecas:'',
        symptome:'',
        sexe:'',
        checkbox:'',
        open:false,
        add:false,
        addCount:0,
        symptoms:[],
        sliderventre:'',
        resultat1:'',
        symptomsChecked:[],
        tempResult:'',
        finalResult:'',
        tabDisease:[],
        doctorList:[],
        doctorSpeciality:[],
        idspeciality:'',
        finalResults:[],
        nontrouve:'',
        consult:'',
        noCheck:''
      
      };
    }
    refresh(){
      window.location.reload()
    }
    demandeRDV(id){
    
      if(window.sessionStorage.getItem("idPatient")=='undefined'){
      alert('Veuillez-vous connecter d\'abord avant de demander un Rendez-vous')
    }
    else if(window.sessionStorage.getItem("idPatient")==null){
      alert('Veuillez-vous connecter d\'abord avant de demander un Rendez-vous')
    }
    else{
      //window.location.href="demandeRDV/"+id;
     // <Redirect to={`/demandeRDV/${id}`} />
     this.props.history.push("demandeRDV/"+id);
  
    }
  }
    handlePdf = () => {
      const input = document.getElementById('pdfdiv');  
      html2canvas(input)  
        .then((canvas) => {  
          var imgWidth = 180;  
          var pageHeight = 210;  
          var imgHeight = canvas.height * imgWidth / canvas.width;  
          var heightLeft = imgHeight;  
          const imgData = canvas.toDataURL('image/png');  
          const pdf = new jsPDF('p', 'mm', 'a4')  
          var position = 10;  
          var heightLeft = imgHeight; 
          pdf.setProperties({
              title: 'Fiche du diagnostic',
              author: 'RYN',
              creator: 'RYN'
          });
          pdf.setFontSize(13);
          pdf.text(5, 5, 'DocFinder__Fiche de résultat du diagnostic'); 
          pdf.setLineWidth(1)
          pdf.addImage(imgData, 'JPEG', 10, position, imgWidth, imgHeight);  
          var string = pdf.output('datauristring');
              var embed = "<embed width='100%' height='100%' src='" + string + "'/>"
              var x = window.open();
              x.document.open();
              x.document.write(embed);
              x.document.close();
              pdf.save('fiche.pdf')
        });  
  };
    componentDidMount(){
     axios.get('http://localhost:8080/symptom/')
      .then(response=>{
        console.log(response.data)
        this.setState({symptoms:response.data})
      })
      .catch(error=>{
        console.log(error)
      })
     }
    useStyles = makeStyles((theme) => ({
      root: {
        flexGrow: 1,
      },
      paper: {
        padding: theme.spacing(2),
        textAlign: 'center',
        color: theme.palette.text.secondary,
      },
    }));
    onChangeAge(e){
      this.setState({
        age:e.target.value
      });
    }
    onChangePartieducorps(e){
      this.setState({
        partieducorps:e.target.value
      });
    }
    onChangeSexe(e){
      this.setState({
        sexe:e.target.value
      });
    }
    onChangeSliderventre= (event, newValue) => {
      this.setState({
          sliderventre:newValue
      });
    }
    onChangeAutrecas(e){
      this.setState({
        autrecas:e.target.value
      });
    }
    onChangeCheckbox(e){
      this.setState({ checkbox:[e.target.name] });
    }
    onChangeSymptome1(e){
      console.log(e.target.value)
      this.setState({  symptome:e.target.value });
    }
    onChangeSymptome(e){
      let symptomsC  = this.state.symptomsChecked;
      if(e.target.checked==true){
        console.log("value="+e.target.value)
        console.log("name="+e.target.name)
        console.log("checked="+e.target.checked)
       
        symptomsC.push(e.target.value) ; 
        console.log(symptomsC)
       this.setState({symptomsChecked:symptomsC,noCheck:'' });
      }
      else{
        symptomsC.pop()
        console.log("name="+e.target.name)
        console.log("checked="+e.target.checked)
        this.setState({noCheck:'Veuillez choisir au moins un symptôme'});
      }
   
    }
  
   handleClose(){
      this.setState({
        open:false
      });
    };
  
    
    addSelect(){
     this.setState({
        addCount:this.state.addCount+1
      });
    
    }
    componentWillUnmount(){
      window.sessionStorage.removeItem("idDiseases")
    }
    onSubmit(e){
      e.preventDefault();
      console.log("length="+this.state.symptomsChecked.length)
      
      if(this.state.symptomsChecked.length>0){
        this.setState({tempResult:''});
          var result='';
          for (var i=0; i < this.state.symptomsChecked.length; i++) {
            axios.post('http://localhost:8080/symptomPerDisease/searchIdSymptom', {"idSymptom":this.state.symptomsChecked[i]})
            .then(response=>{
               if(response.data.length>0){
                result=this.state.tempResult+'_'+response.data[0].idDisease;
                this.setState({ tempResult:result});
                //debut2
                var idD = this.state.tempResult.split("_");
                console.log("taille="+idD.length);
                if(idD.length==2){
                  this.setState({
                    finalResult:idD[1]
                   });
                }
                else{
                  for (var i=0; i < idD.length; i++){
                    //console.log("idD="+idD[i] + " / ");
                    if(idD[i]==idD[i+1]){
                      this.setState({
                        finalResult:idD[i]
                       });
                    }      
                 }
                }
                //console.log("final="+this.state.finalResult)
                
                 //debut
                 if(this.state.finalResult!=''){
                  console.log("final="+this.state.finalResult)
                   axios.get('http://localhost:8080/disease/'+this.state.finalResult)
                   .then(response=>{
                       console.log('idDoctorSpeciality1=',response.data)
                       this.setState({
                         finalResults:response.data,
                         nontrouve:'',
                         result:'',
                         idspeciality:response.data.idDoctorSpeciality
                       }) 
                     
                     const doctorSpeciality={idDoctorSpeciality:this.state.idspeciality}
                     axios.post('http://localhost:8080/doctor/searchByIdspecility',doctorSpeciality)
                     .then(res=>{ console.log(res.data)
                     this.setState({
                       doctorList:res.data,
                       })
                        console.log('idSpeciality',this.state.idspeciality)
                         axios.get('http://localhost:8080/doctorSpeciality/'+this.state.idspeciality)
                         .then(r=>{
                            console.log(r.data)
                         this.setState({
                            doctorSpeciality:r.data,
                            })
                         })
                         .catch(error=>{
                            console.log(error)
                         })
                         
                   })
                   .catch(error=>{
                      console.log(error)
                   })     
                   })
                   .catch(error=>{
                        this.setState({nontrouve:'Maladie non trouvé'})
                        this.setState({consult:'médecin généraliste'})
                     })
                    }
                //fin2
               }
               else{
                this.setState({nontrouve:'Maladie non trouvé'})
                this.setState({consult:'médecin généraliste',finalResult:'test'})
               } 
            })
           
        } 
        
      }
      else{
        this.setState({
         
          partieducorps:'',
          autrecas:'',
          symptome:'',
         checkbox:'',
          open:false,
          add:false,
          addCount:0,
            sliderventre:'',
          resultat1:'',
          symptomsChecked:[],
          tempResult:'',
          finalResult:'',
          tabDisease:[],
          doctorList:[],
          doctorSpeciality:[],
          idspeciality:'',
          finalResults:[],
          nontrouve:'',
          noCheck:'Veuillez choisir au moins un symptôme'
        })
      }
    }
   
render(){
  let isLoggedIn;
  let nontrouveR;
  let consultR;
  let button;
  let paper;
  if(window.sessionStorage.getItem("sessionfirstname")=='undefined'){
      isLoggedIn= <Typography variant="subtitle2">Bonjour Anonyme</Typography>
  }
  else if(window.sessionStorage.getItem("sessionfirstname")==null){
      isLoggedIn= <Typography variant="subtitle2">Bonjour Anonyme</Typography>
  }
  else{
      isLoggedIn=<Typography variant="subtitle2"> {`Bonjour Mr./Mme. ${window.sessionStorage.getItem("sessionfirstname")}`}</Typography>
  }

  if(this.state.finalResult!=''){
    button=<Button variant='contained' size="small" color='primary' style={{"fontSize":"13px","backgroundColor":"#ad0303"}} onClick={this.handlePdf}><GetAppIcon fontSize="large"/> Exporter en PDF</Button>
  } 
  
  if(this.state.nontrouve!=''){
    nontrouveR=  <Alert severity="error">
    <AlertTitle><span style={{color:'red'}}>{this.state.nontrouve}</span></AlertTitle>
  </Alert>
  }
  if(this.state.consult!=''){
    consultR=  
    <span>{this.state.consult}</span>

  }
  if(this.state.finalResult==''){
     paper= <Paper className={this.useStyles}>
             
             <Avatar style={{width:600,height:500,color:'#424141'}} variant="square">
        Le résultat s'affihe ici.
      </Avatar>
   </Paper>
  } 
  else{
    paper= <Paper className={this.useStyles}>
             
    {button}
    <div id="pdfdiv">
    <h4 style={{color:'#363434'}}>Résultat du  diagnostic:</h4> 
      <ListItem >
      
        <div >
        <Alert severity="warning">
        <Typography variant="subtitle2" style={{color:"#787575"}}>Les résultats et les conseils qui vont m'être fornis ne peuvent en aucun cas remplacer une consultation médicale et ne constituent pas un diagnostic. De plus, j'ai bien pris note que la version actuelle de SYMPTO-CHECK peut contenir des erreurs ou des inexistantes.</Typography>
        </Alert><br/>
         <Alert severity="info">
             {isLoggedIn}. Vous avez {this.state.age} ans de sexe {this.state.sexe}
          </Alert><br/>
          <h5 style={{color:'#363434',fontFamily:'Arial'}}>La maladie qui correspond le plus aux symptômes que vous venez d'indiquer est la suivante:</h5>
          
        {nontrouveR}
          
       
          <Alert severity="error">
              <AlertTitle>{ this.state.finalResults.name}</AlertTitle>
              —{ this.state.finalResults.description}
          </Alert>
            
              <ListItemText primary="Conseil d'orientation" />
    
      
      <Alert severity="success">
      
          <AlertTitle style={{"textAlign":"justify",fontSize:14}}>Nous vous conseillons de consulter un(e)<strong> { this.state.doctorSpeciality.name} </strong> <br/>
            -{this.state.doctorSpeciality.description}
            {consultR}   
          </AlertTitle>
          
      </Alert>
      
        </div>
       
      </ListItem>
      </div>
        
      <Divider component="li" />
      <ListItem>
          <ListItemText primary="Voici quelques suggestions"  />
      </ListItem>
      <List >
        { this.state.doctorList.map(doc=>
        <ListItem alignItems="flex-start">
            <ListItemAvatar>
            <Avatar alt="Remy Sharp" src="/static/images/avatar/1.jpg" />
            </ListItemAvatar> 
            <Link to={`/detailMedecin/${doc.id}`}>
              <ListItemText style={{fontSize:13}}
              primary={`Dr. ${doc.firstname} ${doc.lastname} `}
              secondary={
                  <React.Fragment>
                {doc.description}
                  </React.Fragment>
              }
              />
           </Link> 
           <ListItemSecondaryAction>
           <Tooltip onClick={()=>this.demandeRDV(doc.id)}  title="Demande de rendez-vous" aria-label="add">
            <Fab color="primary" >
              <EventAvailableIcon />
            </Fab>
          </Tooltip>
          </ListItemSecondaryAction>
        </ListItem>
        )}
      </List>
  </Paper>
  }
  const {symptoms,errorMsg}=this.state
  return (
    <div>
      <br/> 
      <GrandTitre name="Diagnostic"/>
      <br/>
      <div className="row">
      <div className="col-md-1"></div>
      <div className="col-md-5">
        
          <Paper className={this.useStyles}>
          <h4 style={{color:'#363434'}}>Indiquez votre douleur</h4> 
          <InputLabel id="label"> Veuillez remplir les champs suivants</InputLabel>
          <form style={{"paddingLeft":"50px","paddingRight":"50px"}}onSubmit={this.onSubmit}>
            <TextField
              required
              id="age"
              name="age"
              label="Votre age "
              fullWidth
              type="number"
              value={this.state.age}
              onChange={this.onChangeAge}
              variant="outlined"
            
            />
           
          <RadioGroup required row aria-label="position" name="position" defaultValue="top"  onChange={this.onChangeSexe}>
          <FormControlLabel
            value="masculin"
            control={<Radio color="primary" />}
            label="Masculin"
            labelPlacement="start"
            
          />
          <FormControlLabel 
            value="feminin"
            control={<Radio color="primary" />}
            label="Féminin"
            labelPlacement="start"
          />
         </RadioGroup>
         
           <span style={{color:'red'}}>{this.state.noCheck}</span>
          <InputLabel id="label"> Sélectionner les symptomes de la douleur</InputLabel>
          <Autocomplete
              multiple
              id="checkboxes-tags-demo"
              options={symptoms}
              disableCloseOnSelect
              getOptionLabel={(option) => option.name}
              renderOption={(option, { selected }) => (
                <React.Fragment>
                  <Checkbox
                    icon={<CheckBoxOutlineBlankIcon fontSize="small" />}
                    checkedIcon={ <CheckBoxIcon fontSize="small" />}
                    style={{ marginRight: 8 }}
                    checked={selected}
                    onChange={this.onChangeSymptome}
                    onSelect={this.onChangeSymptome}
                    name={option.name}
                    value={option.id}
                    
                  />
                  {option.name}
                </React.Fragment>
              )}
              fullWidth

              renderInput={(params) => (
                <TextField {...params} variant="outlined" label="Les symptômes de la douleur" placeholder="Symptômes"   value={this.state.symptome} onChange={this.onChangeSymptome1}/>
              )}
          
          />
          
           <br/>
            <Button onClick={this.refresh} variant="contained"  color="default" ><RefreshIcon> </RefreshIcon></Button>
         &nbsp;&nbsp;<Button type="submit" variant="contained" color="primary">
               Valider
           </Button>
      
       
        </form>
        <br/> 
    
        </Paper >
            </div>
         
  
            <div className="col-md-5">
          {paper}
         
          </div>
          <div className="col-md-1"></div>
        </div>
      
     
      <br/> 
      <br/> 
      <br/> 
      <br/> 
      
    </div>
  );
}
}