import React, { Component } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import TitleParagraph from './TitleParagraph'
import { render } from '@testing-library/react';
import GrandTitre from './GrandTitre';
import {Link, Redirect} from 'react-router-dom' 




export default class FinDiagnostic extends Component {
  

  constructor(props) {
    super(props)
    
   
    this.state={
        age:window.localStorage.getItem("age"),
        partieducorps:window.localStorage.getItem("partieducorps"),
        autrecas:window.localStorage.getItem("autrecas"),
        symptome:window.localStorage.getItem("symptom"),
        sexe:window.localStorage.getItem("sexe"),
        checkbox:window.localStorage.getItem("checkbox"),
        brulure:window.localStorage.getItem("brulure"),
        ingestion:window.localStorage.getItem("ingestion"),
        pertevaginale:window.localStorage.getItem("pertevaginale"),
        traumatisme:window.localStorage.getItem("traumatisme"),
        urinerouge:window.localStorage.getItem("urinerouge"),
        voyage:window.localStorage.getItem("voyage"),
        diabete:window.localStorage.getItem("diabete"),
        accouchementrecent:window.localStorage.getItem("accouchementrecent"),
        calculvesicule:window.localStorage.getItem("calculvesicule"), 
        sliderventre:window.localStorage.getItem("sliderventre"),
      };
    
    }
     
  
render(){
  return (
    <div>
      <br/> 
      <GrandTitre name="Diagnostic"/>
      <br/>
      <Grid container spacing={2}>
        
        <Grid item xs={3}>
          
        </Grid>
        <Grid item xs={6}>
          
          
          <TitleParagraph
                sectionCount="02" 
                smallTag=""
                    mainTitle="VALIDATION & CONFIRMATION" 
                    addtitle=""
                    subtitle="La santé et la raison sont les vrais trésors de l'homme "
            />
          <br/>  <br/>  <br/>    
          <Paper >
          <React.Fragment >
                 <Typography variant="h6" style={{"textAlign":"justify","color":"crimson","textAlign":"center"}}>
                   En attente de votre validation
                </Typography>
                <hr/>
                 <Typography variant="subtitle2" style={{"color":"#4f4f4d","textAlign":"justify","paddingLeft":"20px","paddingRight":"20px"}}>
                   Les résultats et les conseils qui vont m'être fornis ne peuvent en aucun cas remplacer une consultation médicale et
                   ne constituent pas un diagnostic. De plus, j'ai bien pris note que la version actuelle de SYMPTO-CHECK peut contenir 
                   des erreurs ou des inexistantes.
                </Typography>
                <br/>    
                <Link to="/resultatDiagnostic" button>
                <Button variant="contained" color="secondary">
                     Accepter
                </Button>
                </Link>
                <br/>    
              </React.Fragment>
        <br/> 
    
        </Paper >
  
        
        </Grid>
        <Grid item xs={3}>
         
        </Grid>
      </Grid>
      <br/> 
      <br/> 
      <br/> 
      <br/> 
      
    </div>
  );
}
}