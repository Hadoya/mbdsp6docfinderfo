import React from 'react'
import Grid from '@material-ui/core/Grid';
function GrandTitre({name}){
    
    return(
      <Grid item xs={12}>
          <div className="inner_page">
          <div className="section inner_page_header">
        <div className="container">
          <div className="row">
              <div className="col-lg-12">
              <div className="full">
                <h3>{name}</h3>
            </div>
          </div>
        </div>
      </div>
    </div>
    </div>
    </Grid>
    )      
}
export default  GrandTitre