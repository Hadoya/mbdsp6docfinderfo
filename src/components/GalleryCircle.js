import React from 'react'
function GalleryCircle(){
    
    return(
        <div class="section layout_padding">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="full">
                        <div class="heading_main text_align_left">
						   <div class="left">
						     <p class="section_count">05</p>
						   </div>
						   <div class="right">
						    <p class="small_tag">Our Team</p>
                            <h2><span class="theme_color">We Have a Professional</span> Team of Business Analysts.</h2>
							</div>	
                        </div>
                    </div>
                </div>
            </div>
			<div class="row margin-top_30">
                <div class="col-lg-12 margin-top_30">
                    <div id="team_slider" class="carousel slide" data-ride="carousel">

                      
                        <div class="carousel-inner">
                            <div class="carousel-item active">
                                <div class="row">
								
                                    <div class="col-lg-4 col-md-4 col-sm-12">
                                        <div class="full">
										   <div class="full team_member_img text_align_center"> 
										      <img src="assets/img/img-7.png" alt="#" />
											 <div class="social_icon_team">
											    <ul class="social_icon">
                                <li><a href="#"><i class="fa fa-facebook-f"></i></a></li>
                                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                               </ul>
                                             </div>											 
										   </div>
										   <div class="full text_align_center">
										     <h3>Jone due</h3>
										   </div>
										   <div class="full text_align_center">
										     <p>this a long established fact that a reader will be distracted by the readable content </p>
										   </div>
										</div>
                                    </div>
									
                                   <div class="col-lg-4 col-md-4 col-sm-12">
                                        <div class="full">
										   <div class="full team_member_img text_align_center"> 
										      <img src="assets/img/img-8.png" alt="#" />
											 <div class="social_icon_team">
											    <ul class="social_icon">
                                <li><a href="#"><i class="fa fa-facebook-f"></i></a></li>
                                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                               </ul>
                                             </div>											 
										   </div>
										   <div class="full text_align_center">
										     <h3>Jone due</h3>
										   </div>
										   <div class="full text_align_center">
										     <p>this a long established fact that a reader will be distracted by the readable content </p>
										   </div>
										</div>
                                    </div>
									
                                   <div class="col-lg-4 col-md-4 col-sm-12">
                                        <div class="full">
										   <div class="full team_member_img text_align_center"> 
										      <img src="assets/img/img-9.png" alt="#" />
											 <div class="social_icon_team">
											    <ul class="social_icon">
                                <li><a href="#"><i class="fa fa-facebook-f"></i></a></li>
                                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                               </ul>
                                             </div>											 
										   </div>
										   <div class="full text_align_center">
										     <h3>Jone due</h3>
										   </div>
										   <div class="full text_align_center">
										     <p>this a long established fact that a reader will be distracted by the readable content </p>
										   </div>
										</div>
                                    </div>
									
                                </div>
                            </div>
                            <div class="carousel-item">
                                <div class="row">
								
                                    <div class="col-lg-4 col-md-4 col-sm-12">
                                        <div class="full">
										   <div class="full team_member_img text_align_center"> 
										      <img src="assets/img/img-7.png" alt="#" />
											 <div class="social_icon_team">
											    <ul class="social_icon">
                                <li><a href="#"><i class="fa fa-facebook-f"></i></a></li>
                                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                               </ul>
                                             </div>											 
										   </div>
										   <div class="full text_align_center">
										     <h3>Jone due</h3>
										   </div>
										   <div class="full text_align_center">
										     <p>this a long established fact that a reader will be distracted by the readable content </p>
										   </div>
										</div>
                                    </div>
									
                                   <div class="col-lg-4 col-md-4 col-sm-12">
                                        <div class="full">
										   <div class="full team_member_img text_align_center"> 
										      <img src="assets/img/img-8.png" alt="#" />
											 <div class="social_icon_team">
											    <ul class="social_icon">
                                <li><a href="#"><i class="fa fa-facebook-f"></i></a></li>
                                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                               </ul>
                                             </div>											 
										   </div>
										   <div class="full text_align_center">
										     <h3>Jone due</h3>
										   </div>
										   <div class="full text_align_center">
										     <p>this a long established fact that a reader will be distracted by the readable content </p>
										   </div>
										</div>
                                    </div>
									
                                   <div class="col-lg-4 col-md-4 col-sm-12">
                                        <div class="full">
										   <div class="full team_member_img text_align_center"> 
										      <img src="assets/img/img-9.png" alt="#" />
											 <div class="social_icon_team">
											    <ul class="social_icon">
                                <li><a href="#"><i class="fa fa-facebook-f"></i></a></li>
                                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                               </ul>
                                             </div>											 
										   </div>
										   <div class="full text_align_center">
										     <h3>Jone due</h3>
										   </div>
										   <div class="full text_align_center">
										     <p>this a long established fact that a reader will be distracted by the readable content </p>
										   </div>
										</div>
                                    </div>
									
                                </div>
                            </div>
                        </div>

                        <div class="bullets">
						   <ul class="carousel-indicators">
                                <li data-target="#team_slider" data-slide-to="0" class="active"></li>
                                <li data-target="#team_slider" data-slide-to="1"></li>
                            </ul>
						</div>

                    </div>
                </div>

            </div>
        </div>
    </div>
	
    )      
}
export default  GalleryCircle